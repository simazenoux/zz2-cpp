#include "Mere.hpp"

int Mere::_compteur = 0;

Mere::Mere(std::string name) : _name(name)
{
    std::cout << "Bonjour, je suis " << _name << ", la maman " << _compteur << std::endl;
    ++_compteur;
}

std::string Mere::getName()
{
    return _name;
}
int Mere::getCompteur()
{
    return _compteur;
}