#ifndef MERE_HPP
#define MERE_HPP

#include <iostream>

class Mere
{
    private:
        static int _compteur;

        std::string _name;

    public:
        Mere(std::string name);

        std::string getName();

        static int getCompteur();
};



#endif