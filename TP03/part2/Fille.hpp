#ifndef FILLE_HPP
#define FILLE_HPP

#include <iostream>

#include "Mere.hpp"

class Fille: public Mere
{
    public:
        Fille(std::string name);
};

#endif