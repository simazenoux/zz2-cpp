#include "ProducteurPremiersEntiers.hpp"

bool ProducteurPremiersEntiers::produire(int quantite, std::string nom) 
{
    std::ofstream fichier(nom);

    fichier << quantite << " ";

    for (int i=1; i<=quantite; i++)
    {
        fichier << i << " ";
    }
    fichier << std::endl;
    
    _travail++;

    return true;
}