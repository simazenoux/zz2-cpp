#ifndef __CPP3_STATISTICIEN_HPP__
#define __CPP3_STATISTICIEN_HPP__

#include <iostream>

class Statisticien {
	
    private:
        bool _calcul;
        int _demande;
        int _somme;
        int _moyenne;

    public:
        Statisticien();
        bool aCalcule();
        int getDemande();
        int getSomme();
        int getMoyenne();
        
        void acquerir(std::string const nom);

};

#endif