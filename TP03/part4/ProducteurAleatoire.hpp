#ifndef PRODUCTEUR_ALEATOIRE_HPP
#define PRODUCTEUR_ALEATOIRE_HPP

#include <random>

#include "producteur.hpp"

class ProducteurAleatoire: public Producteur
{
    public:
        bool produire(int quantite, std::string nom);
};

#endif