#include "statisticien.hpp"
#include <fstream>


Statisticien::Statisticien(): _calcul(false)
{}

bool Statisticien::aCalcule()
{
    return _calcul;
}

int Statisticien::getDemande()
{
    return _demande;
}

int Statisticien::getSomme()
{
    return _somme;
}
int Statisticien::getMoyenne()
{
    return _moyenne;
}

void Statisticien::acquerir(std::string const nom) 
{
    std::ifstream fichier(nom);

    fichier >> _demande; 
    for (int i=0; i<_demande; i++)
    {
        int lecture;
        fichier >> lecture;
        _somme += lecture;
    }

    _moyenne = _somme / _demande;
	
    _calcul = true;
}
