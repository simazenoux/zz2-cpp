#ifndef PRODUCTEUR_PREMIERS_ENTIERS_HPP
#define PRODUCTEUR_PREMIERS_ENTIERS_HPP

#include "producteur.hpp"

class ProducteurPremiersEntiers : public Producteur
{
    public:
        virtual bool produire(int quantite, std::string nom);
};

#endif