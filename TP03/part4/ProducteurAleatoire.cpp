#include "ProducteurAleatoire.hpp"

bool ProducteurAleatoire::produire(int quantite, std::string nom) 
{
    std::ofstream fichier(nom);

    std::random_device r;
    std::default_random_engine e1(r());

    fichier << quantite << " ";

    std::uniform_int_distribution<int> uniform_dist(1, quantite);

    for (int i=1; i<=quantite; i++)
    {
        fichier << uniform_dist(e1) << " ";
    }
    fichier << std::endl;
    
    _travail++;

    return true;
}