#ifndef __CPP3_PRODUCTEUR_HPP__
#define __CPP3_PRODUCTEUR_HPP__

#include <iostream>
#include <fstream>
#include <string>

class Producteur {

    protected:
        int _travail;

    public:
        Producteur();
        virtual ~Producteur();
        int getTravail();
        virtual bool produire(int quantite, std::string nom) = 0;
};

#endif
