#include "B.hpp"
#include "A.hpp"

void B::send(A& a)
{
    a.exec();
}

void B::exec()
{
    std::cout << "B s'exécute" << std::endl;
}