#include "A.hpp"
#include "B.hpp"

void A::send(B& b)
{
    b.exec();
}

void A::exec()
{
    std::cout << "A s'exécute" << std::endl;
}
