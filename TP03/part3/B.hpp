#ifndef B_HPP
#define B_HPP

#include <iostream>

class A;

class B
{
    private:
        int j;

    public:
        void send(A& a);
        void exec();
};

#endif