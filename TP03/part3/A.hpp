#ifndef A_HPP
#define A_HPP

#include <iostream>

class B;

class A
{
    private:
        int i;
    
    public:
        void send(B& b);
        void exec();
};

#endif