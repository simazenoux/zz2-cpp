
#ifndef BAVARDE_HPP
#define BAVARDE_HPP

#include <iostream>

class Bavarde {

    private:
        int _n;

    public:
        Bavarde(int n = 0): _n(n)
        {
            std::cout << "Construction de " << _n << std::endl;
        }
        ~Bavarde()
        {
            std::cout << "Tais toi " << _n << std::endl;
        }

        void afficher()
        {
            std::cout << "Affichage de " << _n << std::endl;
        }

        int getValue()
        {
            return _n;
        }
};  

#endif