#include "Bavarde.cpp"

class Couple
{
    private:
        Bavarde _b1;
        Bavarde _b2;

    public:
        Couple(Bavarde b1, Bavarde b2): _b1(b1), _b2(b2)
        {}
};