#include "Bavarde.cpp"

class Famille
{
    private:
        Bavarde* _p;

    public:
        Famille(int i): _p(new Bavarde[i])
        {}  
        ~Famille()
        {
            delete[] _p;
        }
};