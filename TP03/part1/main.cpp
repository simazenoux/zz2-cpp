#include "Couple.cpp"
#include "Famille.cpp"

Bavarde globale(2);

void fonction(Bavarde) {
  std::cout << "code de la fonction" << std::endl;
}

int main(int, char **) 
{
    Bavarde b;
    Bavarde * p = new Bavarde(3);
    fonction(b);
    delete p;


    const int TAILLE = 20;
    Bavarde   tab1[TAILLE];
    Bavarde * tab2 = new Bavarde[TAILLE];
    for (int i =0; i < TAILLE; ++i) 
    {
        tab1[i].afficher();
        tab2[i].afficher();
    }
    delete [] tab2;

    Couple couple (Bavarde(10), Bavarde(20));

    Famille famille(10);

    return 0;
}