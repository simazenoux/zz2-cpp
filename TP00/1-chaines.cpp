#include <iostream>
#include <sstream>

int main(int, char **)
{
    std::string s1;
    std::string s2 ("chaine 2");
    std::string s3 = "chaine 3";

    std::cout << (s1.empty()?"chaine vide":"chaine non vide");
    
    s2 = s3; // affectation
    std::cout << s2; // affichage sur la sortie standard
    
    
    std::cin  >> s3; // saisie sur l'entrée standard

    // taille des chaînes
    std::cout << s1.length() << std::endl;
    std::cout << s2.size()   << std::endl;

    // vider la chaîne
    s3.clear();

    // comparaisons
    if (s2==s3) std::cout << "les chaines sont egales" ;
    if (s2!=s3) std::cout << "les chaines sont differentes" ;
    if (s2< s3) std::cout << s2 << " est plus petite que " << s3;
    if (s2> s3) std::cout << s2 << " est plus grande que " << s3;
    if (s2.compare(s3)==0) std::cout << "les chaines sont égales" ;
    if (s3.compare("loic")==0) std::cout << "s3 est identifié :-)" ;


    // lecture d'un caractère
    std::cout << s2[3];    // 4eme caractère
    std::cout << s2.at(3); // 4eme caractère

    // modification de caractère
    s2[2]    = 'A';
    s2.at(3) = 'B';

    // même résultat
    s2 += s3;
    s2.append(s3);

    // mecanique interne
    std::cout << s2.c_str();
    std::cout << (int) s2.c_str()[s2.length()];

    // autres opérations
    std::string s;
    s1.insert(3, s2); // insère s2 à la position 3
    s3.erase(0, 4);   // efface les 4 premiers caractères
    s3.erase(1, 2);   // efface deux caractères à partir de la position 1
    s3.replace(2,3,s1); // remplace la portion de s3 définie de la position 2 à 5 par la chaîne s1
    s = s1.substr(4,2); // extrait 2 caractères à partir de la position 4
    unsigned int i = s.find("trouve", 4); // recherche "trouve" à partir de la 4ème position, renvoie std::string::npos le cas échéant


    // streams
    int valeur_entiere;
    std::ostringstream oss;
    // écrire la valeur dans le flux et tout ce que l'on veut
    oss << valeur_entiere;
    std::cout << "Résultat" << oss.str();

    return 0;
}