#include "Point.hpp"

int main(int, char**) {
    Point p1;
    p1.setX(1);
    p1.setY(2);

    Point p2(3, 4);

    Point* p3 = new Point(5, 6);

    std::cout << p1 << std::endl << p2 << std::endl << *p3 << std::endl;

    delete p3;
  
    return 0;
}