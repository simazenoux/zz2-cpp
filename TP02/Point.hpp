#ifndef POINT_HPP
#define POINT_HPP

#include <iostream> 

class Point 
{
    private:
        int _x;
        int _y;
        static int _compteur;

    public:
        Point();
        Point(int x, int y);

        int getX(); 
        int getY();
        int getCompteur();
        void setX(int x);
        void setY(int y);

        void deplacerDe(int x, int y);
        void deplacerVers(int x, int y);

        friend std::ostream& operator<< (std::ostream& out, const Point& point);
};

#endif