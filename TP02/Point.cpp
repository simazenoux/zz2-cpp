#include "Point.hpp"

int Point::_compteur = 0;

Point::Point(int x, int y): _x(x), _y(y)
{
    ++_compteur;

    std::cout << "Point(x, y)" << std::endl;
}


Point::Point(): Point(0,0)
{
    std::cout << "Point()" << std::endl;
}


int Point::getX() {
    return _x;
}

int Point::getY() {
    return _y;
}

int Point::getCompteur() {
    return _compteur;
}

void Point::setX(int x) {
    _x = x;
}

void Point::setY(int y) {
    _y = y;
}

void Point::deplacerDe(int x, int y) {
    setX(getX()+x);
    setY(getY()+y);
}

void Point::deplacerVers(int x, int y) {
    setX(x);
    setY(y);
}

std::ostream& operator<< (std::ostream& out, const Point& point)
{

    out << "Point(" << point._x << ", " << point._y << ')';

    return out;
}