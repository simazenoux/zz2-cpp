#include "catch.hpp"

#include "Individu.hpp"
#include "Humain.hpp"
#include "Elfe.hpp"
#include "Orc.hpp"
#include "Chaman.hpp"

TEST_CASE("Humain") 
{
    std::string nom = "Michel";
    uint intelligence = 50;
    uint force = 50;
    uint vitalite = 50;
    uint magie = 50;

    const Humain humain (nom);

    CHECK( nom          == humain.getNom()          );
    CHECK( intelligence == humain.getIntelligence() );
    CHECK( vitalite     == humain.getVitalite()     );
    CHECK( magie        == humain.getMagie()        );
}


TEST_CASE("Elfe") {
    std::string nom = "Legolas";
    uint intelligence = 100;
    uint force = 100;
    uint vitalite = 100;
    uint magie = 100;

    const Elfe elfe (nom);

    CHECK( nom          == elfe.getNom()          );
    CHECK( intelligence == elfe.getIntelligence() );
    CHECK( vitalite     == elfe.getVitalite()     );
    CHECK( magie        == elfe.getMagie()        );
}


TEST_CASE("Orc") {
    std::string nom = "Azog";
    uint intelligence = 100;
    uint force = 1000;
    uint vitalite = 100;
    uint magie = 0;
    const Orc orc (nom);

    CHECK( nom          == orc.getNom()          );
    CHECK( intelligence == orc.getIntelligence() );
    CHECK( vitalite     == orc.getVitalite()     );
    CHECK( magie        == orc.getMagie()        );
}

TEST_CASE("Chaman") {
    std::string nom = "Generique";
    uint intelligence = 100;
    uint force = 100;
    uint vitalite = 100;
    uint magie = 100;

    const Chaman chaman (nom);

    CHECK( nom          == chaman.getNom()          );
    CHECK( intelligence == chaman.getIntelligence() );
    CHECK( vitalite     == chaman.getVitalite()     );
    CHECK( magie        == chaman.getMagie()        );
}


TEST_CASE("Sorciers") {

    std::vector<Sorciers> sorciers {Chaman("A"), Mage("B")};
    
    for (auto& sorcier:sorciers)
    {
        sorcier.combattre();
    }
}