#include "Individu.hpp"

Individu::Individu(std::string nom, uint intelligence, uint force, uint vitalite, uint magie):
    _nom(nom), _intelligence(intelligence), _force(force), _vitalite(vitalite)
{}


std::string Individu::getNom() const
{
    return _nom;
}

uint Individu::getIntelligence() const
{
    return _intelligence;
}


uint Individu::getVitalite() const
{
    return _vitalite;
}

uint Individu::getMagie() const
{
    return _magie;
}

