#ifndef CHAMAN_HPP
#define CHAMAN_HPP

#include "Elfe.hpp"
#include "Sorcier.hpp"

class Chaman : public Elfe, public Sorcier
{
    public:
        Chaman(const std::string& nom);
}

#endif