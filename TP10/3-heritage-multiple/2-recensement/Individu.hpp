#ifndef INDIVIDU_HPP
#define INDIVIDU_HPP

#include <string>

class Individu
{
    private:
        std::string _nom;
        uint _intelligence;
        uint _force;
        uint _vitalite;
        uint _magie;

    protected:
        Individu(const std::string& nom, uint intelligence, uint force, uint vitalite, uint magie);

    public:
        std::string getNom() const;
        uint getIntelligence() const;
        uint getVitalite() const;
        uint getMagie() const;
};

#endif