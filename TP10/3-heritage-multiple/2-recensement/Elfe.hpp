#ifndef ELFE_HPP
#define ELFE_HPP

#include "Individu.hpp"

class Elfe : public Individu
{

    public:
        Elfe(const std::string nom);
};

#endif