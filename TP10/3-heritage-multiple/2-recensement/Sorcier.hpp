#ifndef SORCIER_HPP
#define SORCIER_HPP



class Sorcier
{
    virtual void ensorceler() = 0;
};

#endif