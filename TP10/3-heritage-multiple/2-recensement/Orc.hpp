#ifndef ORC_HPP
#define ORC_HPP

#include "Individu.hpp"

class Orc: public Individu
{
    public:
        Orc(const std::string& nom);
};

#endif