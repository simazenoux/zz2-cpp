#ifndef HUMAIN_HPP
#define HUMAIN_HPP

#include "Individu.hpp"

class Humain: public Individu
{
    public:
        Humain(const std::string& nom);
};

#endif