#ifndef HUMANOIDE_HPP
#define HUMANIODE_HPP

#include "Humain.hpp"
#include "Machine.hpp"


class Humanoide: public Humain, public Machine
{

    public:
        Humanoide(std::string nom, std::string type, Genre genre, uint age);

};

#endif