#include "Machine.hpp"

Machine::Machine(const std::string& type, uint autonomie, uint ifixit): _type(type), _autonomie(autonomie), _ifixit(ifixit)
{}

std::string Machine::getType() const
{
    return _type;
}
uint Machine::getAutonomie() const
{
    return _autonomie;
}
uint Machine::getIfixit() const
{
    return _ifixit;
}