#ifndef MACHINE_HPP
#define MACHINE_HPP

#include <string>

class Machine
{
    private:
        std::string _type;
        uint _autonomie;
        uint _ifixit;

    public:
        Machine(const std::string& type, uint autonomie, uint ifixit);
        std::string getType() const;
        uint getAutonomie() const;
        uint getIfixit() const;


};

#endif