#ifndef HUMAIN_HPP
#define HUMAIN_HPP

#include <string>

enum Genre
{
    HOMME,
    FEMME
};

class Humain
{
    private:
        std::string _nom;
        Genre _genre;
        uint _age;

    public:
        Humain(const std::string& nom, Genre genre, uint age);
        std::string getNom() const;
        Genre getGenre() const;
        uint getAge() const;
        void setNom(const std::string nom);
        void setGenre(Genre genre);
        void setAge(uint age);
        
};

#endif