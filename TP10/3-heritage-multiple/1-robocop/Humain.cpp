#include "Humain.hpp"

Humain::Humain(const std::string& nom, Genre genre, uint age): _nom(nom), _genre(genre), _age(age)
{}

std::string Humain::getNom() const
{
    return _nom;
}

Genre Humain::getGenre() const
{
    return _genre;
}

uint Humain::getAge() const
{
    return _age;
}

void Humain::setNom(const std::string nom)
{
    _nom = nom;
}


void Humain::setGenre(Genre genre)
{
    _genre = genre;
}

void Humain::setAge(uint age)
{
    _age = age;
}