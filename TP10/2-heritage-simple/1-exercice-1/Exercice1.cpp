#include <iostream>

class Mere 
{
    protected:
        int * tab;

    public:
        Mere()
        {
            // std::cout << "Mere::Mere()" << std::endl;
            tab = new int[100];
        }

        virtual ~Mere() 
        {
            std::cout << "Mere::~Mere()" << std::endl;
            delete [] tab;
        }
};

class Fille : public Mere 
{
    protected:
        double * tab;

    public:
        Fille() 
        {
            // cout << "Fille:Fille()" << endl;
            tab = new double[100];
            //Mere::tab[50] = 4;
        }

        ~Fille() override
        {
            std::cout << "Fille::~Fille()" << std::endl;
            delete [] tab;
        }

};




int main(int, char**)
{
    Mere * m = new Fille();

    std::cout << "penser a valgrind" << std::endl;
    delete m; 

    return 0;
}
