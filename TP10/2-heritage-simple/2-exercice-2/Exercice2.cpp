#include <iostream>

class Mere 
{
    public:
        Mere() { m(); }
        virtual void m() { std::cout << "mm" << std::endl; }
        virtual ~Mere() {}
};

class Fille : public Mere 
{
    public:
        Fille() { m(); }
        virtual void m() { std::cout << "mf" << std::endl; }
        virtual ~Fille() {}
};


int main(int, char**)
{
    Mere * o = new Fille;
    o->m();

    delete o;

    return 0;
}