#include <iostream>

void exemple1()
{
    int   a = 4;
    int * p = nullptr;

    p = &a;
    std::cout << *p << " " << p;
}


void exemple2()
{
    int * p = new int;

    *p = 3;
    std::cout << *p << std::endl;

    delete p;
}

void exemple3()
{
    const int TAILLE = 500;

   int * p = new int[TAILLE];

   for(auto i = 0; i< TAILLE; ++i ) p[i] = i;
   for(auto i = 0; i< TAILLE; ++i ) std::cout << p[i] << std::endl;
       
   delete [] p;
}


int main(int, char**) 
{
   exemple1();
   exemple2();
   exemple3();

   return 0;
}