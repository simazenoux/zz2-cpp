#include <iostream>

void fonction1(int a) {
    std::cout << &a << std::endl;
}

void fonction2(int& a) {
    std::cout << &a << std::endl;
}

void echanger(int* a, int* b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void echanger(int& a, int& b)
{
    int tmp = a;
    a = b; 
    b = tmp;
}

int main(int, char **) {
    int age = 45;

    std::cout << &age << std::endl;
    fonction1(age);
    fonction2(age);
    
    int  a = 3;
    int  b = a;
    int& c = a;

    std::cout << a << " " << b << " " << c << std::endl;
    b = 4;
    std::cout << a << " " << b << " " << c << std::endl;
    c = 5;
    std::cout << a << " " << b << " " << c << std::endl;
    a = 6;
    std::cout << a << " " << b << " " << c << std::endl;


    int x = 1;
    int y = 2;

    std::cout << x << " " << y << std::endl;
    echanger(&x,&y);
    std::cout << x << " " << y << std::endl;
    echanger(x,y);
    std::cout << x << " " << y << std::endl;



  return 0;
}