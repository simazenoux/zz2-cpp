#include "ListeMotsTries.hpp"

void ListeMotsTries::ajouterMot(const std::string& inMot)
{
    _motsTries.emplace_back(inMot);
    _motsTries.sort();
}


size_t ListeMotsTries::getNbMots()
{
    return _motsTries.size();
}

void ListeMotsTries::afficher(std::ostream & flux = std::cout) const
{
    std::copy (_motsTries.begin(), _motsTries.end(), std::ostream_iterator< std::string > (flux, "\n"));
}

std::ostream& operator<<(std::ostream& os, const ListeMotsTries& listeMotsTries)
{
    listeMotsTries.afficher(os);
    return os;
}

std::list < std::string >::const_iterator ListeMotsTries::debut()
{
    return _motsTries.begin();
}

std::list < std::string >::const_iterator ListeMotsTries::fin()
{
    return _motsTries.end();
}

void ListeMotsTries::inserer(const std::list < std::string >::const_iterator & inDebut, const std::list < std::string >::const_iterator & inFin)
{
    for (auto it = inDebut; it != inFin; ++it)
    {
        ajouterMot(*it);
    }

}