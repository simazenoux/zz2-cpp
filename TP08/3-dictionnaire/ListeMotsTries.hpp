#ifndef LISTE_MOTS_TRIES_HPP
#define LISTE_MOTS_TRIES_HPP

#include <iostream>
#include <iterator>
#include <list>
#include <string>

// using ListeMotsTries = std::list< std::string >;

class ListeMotsTries
{

    private:
        std::list< std::string > _motsTries;


    public:
        void ajouterMot(const std::string&);
        size_t getNbMots();
        void inserer(const std::list < std::string >::const_iterator & inDebut, const std::list < std::string >::const_iterator & inFin);
        void afficher(std::ostream & = std::cout) const;
        std::list < std::string >::const_iterator debut();
        std::list < std::string >::const_iterator fin();
        using const_iterator = std::list< std::string >::const_iterator;
};

#endif