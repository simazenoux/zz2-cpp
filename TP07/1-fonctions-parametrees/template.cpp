#include <iostream>

template <typename T>
const T& maximum(const T&a, const T&b) {
    return ((a > b) ? a : b);
}


int main()
{
    std::cout << maximum(5,6) << std::endl;
    return 0;
}