#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <array>
#include <iostream>
#include <stdio.h>
#include <string.h>

template <typename T>
class PileGen
{
    private:
        T* _tab;
        int _size;
        int _capacity;
    
    public:
        PileGen();
        PileGen(int capacity);
        PileGen(T* tab, int size, int capacity);
        PileGen(const PileGen&);
        ~PileGen();

        int capacity() const;
        int size() const;
        bool empty() const;
        void push(T v);
        void pop();
        T top();

        T& operator[](int index) const;
};


template <typename T>
PileGen<T>::PileGen(T* tab, int size, int capacity): _tab(tab), _size(size), _capacity(capacity)
{
    if (_capacity <= 0)
    {
        throw std::invalid_argument("size");
    }
}

template <typename T>
PileGen<T>::PileGen(int capacity): PileGen(capacity <= 0? throw std::invalid_argument("capacity") : new T[capacity], 0, capacity)
{}


template <typename T>
PileGen<T>::PileGen(): PileGen(10)
{}


template <typename T>
PileGen<T>::PileGen(const PileGen& p): PileGen(p._capacity)
{
    _size = p._size;
    memcpy(p._tab, _tab, _size*sizeof(T));
}


template <typename T>
PileGen<T>::~PileGen()
{
    delete [] _tab;
}

template <typename T>
int PileGen<T>::capacity() const
{
    return _capacity;
}



template <typename T>
int PileGen<T>::size() const
{
    return _size;
}


template <typename T>
bool PileGen<T>::empty() const
{
    return _size == 0;
}


template <typename T>
void PileGen<T>::push(T v)
{
    if (_size == _capacity)
    {
        _capacity *= 2;
        _tab = (T*) realloc(_tab, _capacity);
    }

    _tab[_size++] = v;
}


template <typename T>
void PileGen<T>::pop()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("size");
    }
    _size--;
}

template <typename T>
T PileGen<T>::top()
{
    return _tab[_size-1];
}

template <typename T>
T& PileGen<T>::operator[](int index) const
{
    return _tab[index];
}


template <typename T>
std::ostream& operator<< (std::ostream& out, const PileGen<T>& pile)
{
    for (int i=0; i< pile.size(); i++)
    {
        out << pile[i];
        out << " ";
    }
    
    return out;
}


// std::ostream& operator<< (std::ostream& out, const PileGen& pile);

#endif