#ifndef CELL_HPP
#define CELL_HPP

template<typename T>
class Cell
{
    public:
        Cell* _next;
        T _v;

        Cell(Cell* next, T _v);
};


template<typename T>
Cell<T>::Cell(Cell* next, T v): _next(next), _v(v)
{}

#endif