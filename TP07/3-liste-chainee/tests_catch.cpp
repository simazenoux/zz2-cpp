#include "catch.hpp"
#include "List.hpp"

TEST_CASE("Constructeur par defaut") {
   List<int> l;
   
   CHECK(  l.empty() );
   CHECK(  0 == l.size() );
}


TEST_CASE("Test push") {

   List<int> l;
   l.push_front(1);
   
   CHECK(  1 == l.size() );
   CHECK(  1 == l.front() );
   CHECK(  1 == l.back());
}

TEST_CASE("Test push pop") {

   List<int> l;
   l.push_front(1);
   l.pop_front();
   
   CHECK(  l.empty() );
   CHECK(  0 == l.size() );

   l.push_front(1);
   l.pop_back();
   
   CHECK(  l.empty() );
   CHECK(  0 == l.size() );
}

/*
TEST_CASE("Test plusieurs push") {

   List<int> l;
   int size = 4;
   for (int i=1; i<=size; i++)
   {
      l.push_back(i);
   }

   
   CHECK(  !l.empty() );
   CHECK(  size == l.size() );

   CHECK(  1 == l.front() );
   CHECK(  size == l.back() );

}
*/

/*
// A faire quand on aura vu les exceptions
TEST_CASE("Exception pile vide") {

   REQUIRE_THROWS_AS( PileGen<int>().pop(), std::invalid_argument& );
   
}

TEST_CASE("Live pile") {
    PileGen<int> p(10);

    CHECK(  p.empty() );
    CHECK(  0 == p.size() );

    p.push(5);

    CHECK( !p.empty() );
    CHECK( 1 == p.size() );
    CHECK( 5 == p.top() );

    p.push(2);
    p.push(1);

    CHECK( 3 == p.size() );
    CHECK( 1 == p.top() );

    p.pop();

    CHECK( 2 == p.size() );
    CHECK( 2 == p.top() );

    p.pop();
    p.pop();

    CHECK( 0 == p.size() );

}
*/