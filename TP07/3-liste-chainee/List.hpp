#ifndef LIST_HPP
#define LIST_HPP

#include "Cell.hpp"

template<typename T>
class List
{
    private:
        Cell<T>* _first;
        Cell<T>* _last;
        int _size;
        
        void pop(Cell<T>** pprec);
        void push(Cell<T>** pprec, T v);
        

    public:
        List();
        ~List();
        bool empty();
        void push_back(T v);
        void push_front(T v);
        T front();
        T back();
        void pop_front();
        void pop_back();
        unsigned int size();
};

template<typename T>
List<T>::List(): _first(nullptr), _last(nullptr), _size(0)
{}

template<typename T>
List<T>::~List()
{
    while(!empty())
    {
        pop(&_first);
    }
}


template<typename T>
bool List<T>::empty()
{
    return _size == 0;
}


template<typename T>
void List<T>::push_front(T v)
{
    push(&_first, v);
    if (!_last)
    {
        _last = _first;
    }

}


template<typename T>
void List<T>::push_back(T v)
{   
    push(&_last, v);
    if (!_first)
    {
        _first = _last;
    }
}


template<typename T>
T List<T>::front()
{
    return _first->_v;
}


template<typename T>
T List<T>::back()
{
    return _last->_v;
}


template<typename T>
void List<T>::pop_front()
{
    pop(&_first);
}


template<typename T>
void List<T>::pop_back()
{
    pop(&_last);
}


template<typename T>
unsigned int List<T>::size()
{
    return _size;
}


template<typename T>
void List<T>::pop(Cell<T>** pprec)
{
    if (*pprec)
    {
        Cell<T>* tmp = (*pprec)->_next;
        delete *pprec;
        *pprec = tmp;
        _size--;
    }
}


template<typename T>
void List<T>::push(Cell<T>** pprec, T v)
{
    Cell<T>* cell = new Cell<T>(*pprec, v);
    *pprec = cell;
    _size++;
}

#endif