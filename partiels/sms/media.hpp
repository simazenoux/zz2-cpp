#ifndef MEDIA_HPP
#define MEDIA_HPP

#include <string>

class Media
{
    public:
        virtual ~Media();
        virtual std::string afficher() = 0;
};

#endif