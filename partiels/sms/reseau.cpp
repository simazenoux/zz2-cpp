#include "reseau.hpp"


std::string Reseau::lister()
{
    std::string res = "";
    // for (size_t i=0; i<telephones.size(); i++)
    // {
    //     res+=telephones[i].getNumero() + "\n";
    //     res += telephones[i].ke
    // }

    for (const auto& n : telephones) 
    {
        res += n.first + "\n";
    }
    return res;
}

void Reseau::ajouter(std::string numero)
{
    telephones[numero] = Telephone(numero, this);
}


Telephone& Reseau::trouveTel(std::string numero)
{
    if (telephones.count(numero) == 0)
    {
        throw MauvaisNumero();
    }
    return telephones[numero];
}