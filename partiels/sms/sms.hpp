#ifndef __SMS
#define __SMS

#include "message.hpp"

class SMS: public Message
{
    private:
        std::string _texte;

    public:
        SMS(std::string de, std::string a, std::string date);

        virtual std::string afficher() override;
        std::string getTexte();

        void setTexte(std::string texte);
};

#endif