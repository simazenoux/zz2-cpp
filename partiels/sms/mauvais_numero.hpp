#ifndef MAUVAIS_NUMERO_HPP
#define MAUVAIS_NUMERO_HPP

#include <exception>

class MauvaisNumero : public std::invalid_argument
{
    public:
        MauvaisNumero(): std::invalid_argument("mauvais numero")
        {}

};

#endif