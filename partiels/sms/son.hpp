#ifndef SON_HPP
#define SON_HPP

#include "media.hpp"

class Son: public Media
{
    public:
        virtual std::string afficher() override;
};

#endif