#include "telephone.hpp"

Telephone::Telephone(std::string numero, Reseau* reseau): _numero(numero), _reseau(reseau), _nbMessages(0)
{}

Telephone::Telephone(std::string numero): Telephone(numero, NULL)
{}

Telephone::Telephone(): Telephone("")
{}



Reseau* Telephone::getReseau() const
{
    return _reseau;
}

std::string Telephone::getNumero() const
{
    return _numero;
}

uint Telephone::getNbMessages() const
{
    return _nbMessages;
}




void Telephone::setNumero(std::string numero)
{
    _numero = numero;
}

void Telephone::setReseau(Reseau& reseau)
{
    _reseau = &reseau;   
}

void Telephone::setNbMessages(uint nbMessages)
{
    _nbMessages = nbMessages;
}

void Telephone::incrementerNbMessages()
{
    _nbMessages++;
}




void Telephone::textoter(std::string a, std::string texte)
{
    SMS sms(_numero, a, "");
    sms.setTexte(texte);
    
    incrementerNbMessages();

    try
    {
        _reseau->trouveTel(a).incrementerNbMessages();
    }
    catch (std::exception& e)
    {}
    
}


void Telephone::mmser(std::string a, MMS * mms)
{
    mms->setDe(_numero);
    mms->setA(a);
    textoter(a, mms->getTexte());
}

