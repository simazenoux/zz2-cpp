#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <string>

class Message
{
    private:
        std::string _de;
        std::string _a;
        std::string _date;
        int _id;
        static int _cle;

    public:
        Message(std::string de, std::string a, std::string date);
        virtual ~Message();

        virtual std::string afficher() = 0;

        std::string getDe();
        std::string getA();
        std::string getDate();
        int getId();
        static int getCle();

        void setDe(std::string de);
        void setA(std::string a);

};

#endif