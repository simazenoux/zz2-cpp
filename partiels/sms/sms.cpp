#include "sms.hpp"


SMS::SMS(std::string de, std::string a, std::string date): Message(de,a,date)
{}


std::string SMS::getTexte()
{
    return _texte;
}

std::string SMS::afficher()
{
    return getTexte();
}


void SMS::setTexte(std::string texte)
{
    _texte = texte;
}

