#ifndef RESEAU_HPP
#define RESEAU_HPP

#include <bits/stdc++.h>
#include <vector>
#include <map>

#include "telephone.hpp"

class Telephone;

class Reseau
{
    private:
        std::map<std::string, Telephone> telephones;

    public:
        std::string lister();
        void ajouter(std::string numero);
        Telephone& trouveTel(std::string numero);

};

#endif