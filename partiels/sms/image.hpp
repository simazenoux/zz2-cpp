#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "media.hpp"

class Image: public Media
{
    public:
        virtual std::string afficher() override;
};

#endif