#ifndef MMS_HPP
#define MMS_HPP

#include <sstream> 
#include <vector>

#include "sms.hpp"
#include "media.hpp"

class MMS: public SMS
{
    private:
        std::vector<Media*> _medias;

    public:
        MMS(std::string de, std::string a, std::string date);
        virtual ~MMS();

        virtual std::string afficher() override;
        void joindre(Media* media);
};


#endif