#include "message.hpp"


int Message::_cle = 0;



Message::Message(std::string de, std::string a, std::string date): _de(de), _a(a), _date(date), _id(_cle++)
{}

Message::~Message()
{}


std::string Message::getDe()
{
    return _de;
}

std::string Message::getA()
{
    return _a;
}

std::string Message::getDate()
{
    return _date;
}

int Message::getId()
{
    return _id;
}

int Message::getCle()
{
    return _cle;
}


void Message::setDe(std::string de)
{
    _de = de;
}

void Message::setA(std::string a)
{
    _a = a;
}