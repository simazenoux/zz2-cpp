#include "mms.hpp"


MMS::MMS(std::string de, std::string a, std::string date): SMS(de,a,date)
{}

MMS::~MMS()
{
    for (size_t i=0; i<_medias.size(); i++)
    {
        delete(_medias[i]);
    }
}


std::string MMS::afficher()
{
    std::ostringstream oss;
    oss << getTexte();
    for (size_t i=0; i<_medias.size(); i++)
    {
        oss << _medias[i]->afficher();
    }
    return oss.str();
}


void MMS::joindre(Media* media)
{
    _medias.push_back(media);
}