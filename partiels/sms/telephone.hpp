#ifndef TELEPHONE_HPP
#define TELEPHONE_HPP

#include <string>

#include "reseau.hpp"
#include "mauvais_numero.hpp"
#include "sms.hpp"
#include "mms.hpp"

class Reseau;

class Telephone
{
    private:
        std::string _numero;
        Reseau * _reseau;
        uint _nbMessages;

    public:
        Telephone();
        Telephone(std::string numero);
        Telephone(std::string numero, Reseau* reseau);

        std::string getNumero() const;
        Reseau* getReseau() const;
        uint getNbMessages() const;

        void setReseau(Reseau& reseau);
        void setNumero(std::string numero);
        void setNbMessages(uint nbMessages);

        void incrementerNbMessages();

        void textoter(std::string a, std::string texte);
        void mmser(std::string a, MMS * mms);
        
};

inline bool operator< (const Telephone& lhs, const Telephone& rhs)
{
return lhs.getNumero() < rhs.getNumero();
}

#endif