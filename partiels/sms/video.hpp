#ifndef VIDEO_HPP
#define VIDEO_HPP

#include "media.hpp"

class Video:public Media
{
    public:
        virtual std::string afficher() override;
};

#endif