#ifndef CPP5__CHAINE_HPP
#define CPP5__CHAINE_HPP

#include <exception>
#include <fstream>
#include <iostream>

class null_pointer: public std::logic_error{};
    
class Chaine  {


    private:
        int _capacite;
        char * _tab;

    public:
        Chaine();
        Chaine(const char * inCS);
        Chaine(int inCapacite);
        Chaine(const Chaine& chaine);
        Chaine(const char * inCS, int capacite);
        ~Chaine();

        int getCapacite() const;
        char * c_str() const;

        void afficher(std::ostream & ss = std::cout) const;

        Chaine& operator=(const Chaine &uC);
        char& operator[](int index);

        friend Chaine operator+(const Chaine &c1, const Chaine &c2);
};

std::ostream& operator<< (std::ostream& out, const Chaine& chaine);

#endif
