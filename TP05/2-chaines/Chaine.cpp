#include "Chaine.hpp"
#include <cstring>


Chaine::Chaine(const char * inCS, int capacite): 
    _capacite(capacite), 
    _tab(_capacite<0 ? nullptr : new char[_capacite+1])
{
    if (capacite > 0)
    {
        strcpy(_tab, inCS);
    }
}


Chaine::Chaine(const char * inCS): Chaine(strlen(inCS) == 0? "\0" : inCS, strlen(inCS))
{}


Chaine::Chaine(int inCapacite): Chaine("\0", inCapacite)
{}


Chaine::Chaine(): Chaine(nullptr, -1)
{}

Chaine::Chaine(const Chaine& chaine): Chaine(chaine.c_str(), chaine.getCapacite())
{
    std::cerr<< "Constructeur de copie appelé" << std::endl;
}



Chaine::~Chaine()
{
    delete [] _tab;
}

int Chaine::getCapacite() const
{
    return _capacite;
}

char * Chaine::c_str() const
{
    return _tab;
}

void Chaine::afficher(std::ostream & ss) const
{
    ss << c_str();
}



Chaine& Chaine::operator=(const Chaine &uC) {
    if (&uC != this) 
    {
        delete [] _tab;
        _capacite = uC.getCapacite();
        if (_capacite)
        {
            _tab = new char[_capacite];
            strcpy(_tab, uC.c_str());
        }
        else
        {
            _tab = 0;
        }
    }
    return *this;
}

char& Chaine::operator[](int index)
{
    if (index < 0)
    {
        throw std::out_of_range("index");
    }
    if (index > _capacite)
    {
        throw std::bad_alloc();
    }
    return _tab[index];
}


Chaine operator+(const Chaine &c1, const Chaine &c2)
{
    Chaine* chaine = new Chaine(strlen(c1.c_str()) + strlen(c2.c_str()));
    strcat(chaine->_tab, c1.c_str());
    strcat(chaine->_tab, c2.c_str());

    return *chaine;
}

/*
Chaine operator+(const Chaine &c1, const Chaine &c2)
{
    Chaine chaine(strlen(c1.c_str()) + strlen(c2.c_str()));
    strcat(chaine._tab, c1.c_str());
    strcat(chaine._tab, c2.c_str());

    return chaine;
*/


std::ostream& operator<< (std::ostream& out, const Chaine& chaine)
{
    out << chaine.c_str();
    return out;
}
