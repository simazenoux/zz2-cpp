#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <array>
#include <iostream>
#include <stdio.h>
#include <string.h>

class Vector
{
    private:
        int* _tab;
        int _size;
        int _capacity;
    
    public:
        Vector();
        Vector(int capacity);
        Vector(int* tab, int size, int capacity);
        Vector(const Vector&);
        ~Vector();

        int capacity() const;
        int size() const;
        void push_back(int v);

        int operator[](int const index) const;
        int& operator[](int const index);

        Vector& operator=(const Vector&);
        friend Vector operator+(const Vector& v1, const Vector& v2);
};

std::ostream& operator<< (std::ostream& out, const Vector& vector);

#endif