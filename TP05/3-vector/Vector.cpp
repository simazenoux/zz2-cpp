#include "Vector.hpp"

Vector::Vector(int* tab, int size, int capacity): _tab(tab), _size(size), _capacity(capacity)
{}

Vector::Vector(int capacity): Vector(new int[capacity], 0, capacity)
{}

Vector::Vector(): Vector(10)
{}

Vector::Vector(const Vector& v): Vector(v._capacity)
{
    _size = v._size;
    memcpy(v._tab, _tab, _size*sizeof(int));
}

Vector::~Vector()
{
    delete [] _tab;
}


int Vector::capacity() const
{
    return _capacity;
}

int Vector::size() const
{
    return _size;
}

void Vector::push_back(int v)
{
    if (_size == _capacity)
    {
        _capacity *= 2;
        _tab = (int*) realloc(_tab, _capacity);
    }

    _tab[_size++] = v;
}

int Vector::operator[](int const index) const
{
    return _tab[index];
}


int& Vector::operator[](int const index)
{
    return _tab[index];
}



Vector& Vector::operator=(const Vector& v)
{
    if (&v != this)
    {
        delete [] _tab;
        memcpy(_tab, v._tab, v.size() * sizeof(int));
        _size = v.size();
        _capacity = v.capacity();
    }
    return *this;
}

Vector operator+(const Vector& v1, const Vector& v2)
{
    Vector v (v1.size() + v2.size());
    v._size += v1.size() + v2.size();
    memcpy(v._tab, v1._tab, v1.size() * sizeof(int));
    memcpy(v._tab + v1.size(), v2._tab, v2.size() * sizeof(int));

    return v;

}

std::ostream& operator<< (std::ostream& out, const Vector& vector)
{
    for (int i=0; i<vector.size(); i++)
    {
        out << vector[i];
        out << " ";
    }
    
    return out;
}
