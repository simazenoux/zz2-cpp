#include "catch.hpp"
#include "Vector.hpp"

TEST_CASE("Constructeur par defaut") {
   Vector v;
   CHECK( 0 == v.size());
   CHECK( 10 == v.capacity());
}



TEST_CASE("Verification des const sur les accesseurs") {
   const Vector v;
   CHECK( 0 == v.size());
   CHECK( 10 == v.capacity());
//    CHECK(v1[0])
}


/*
TEST_CASE("Constructeur par chaine C") {
	char  source []= "rien";
    Chaine c1(source);
    CHECK( (signed)strlen(source) == c1.getCapacite() );
    CHECK( 0 == strcmp(source, c1.c_str()) ); 

    Chaine c2 = "";
    CHECK( 0 == c2.getCapacite() );
    CHECK( 0 == strcmp("", c2.c_str())); 
} 
*/


TEST_CASE("Constructeur avec capacite") {
    Vector v1(6);
    CHECK( 6 == v1.capacity());
    CHECK( 0 == v1.size()); 
}



TEST_CASE("Constructeur de copie") {
    Vector v0;
    Vector v1(10);   // une chaine vide
    // int* tab = {1,2,3};
    // Vector v2(tab, 3, 10);
    v1.push_back(123);
    // v1[0] = 123;

    Vector v2 = v1;  // une autre chaine vide
    
    CHECK( v1.capacity() == v2.capacity() );
    CHECK( v1.size() == v2.size() );
    // tous les problemes vont venir de la
    // j'ai converti en void * uniquement pour l'affichage de catch
    CHECK( (void *) &(v1[0]) != (void *) &(v2[0]) );
    // CHECK(           v1[0] ==           v2[0] );
    // CHECK( 0 == strcmp(s1.c_str(), s2.c_str() ));
}


/*
TEST_CASE("methode afficher") {
	const char * original = "une chaine a tester";
    const Chaine c1(original);
    std::stringstream ss;
    
    
    c1.afficher(); // on verifie juste que ca compile
    c1.afficher(ss);

    CHECK( ss.str() == original); // test de std::string :-)
}



TEST_CASE("operateur d'affectation") {
	Chaine s1("une premiere chaine");
    Chaine s2("une deuxieme chaine plus longue que la premiere");
    
    s1 = s2;

    CHECK( s1.getCapacite() == s2.getCapacite()); 
    CHECK( (void *)s1.c_str() != (void *)s2.c_str() );
    CHECK( 0 == strcmp(s1.c_str(), s2.c_str() ));

    s1 = s1; // est ce que cela va survivre a l execution ?
}
*/

TEST_CASE("Surcharge <<") {
	Vector v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    std::stringstream ss;
    ss << v;

    CHECK( ss.str() == "1 2 3 ");
}



TEST_CASE("operateur d'addition") {
	Vector v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    Vector v2;
    v2.push_back(4);
    v2.push_back(5);

    Vector v = v1 + v2;

    std::stringstream ss;
    ss << v;

    CHECK( 0 == strcmp(ss.str().c_str(), "1 2 3 4 5 "));
    CHECK(v.capacity() == v1.size() + v2.size());
}