#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <array>
#include <iostream>
#include <stdio.h>
#include <string.h>

class Pile
{
    private:
        int* _tab;
        int _size;
        int _capacity;
    
    public:
        Pile();
        Pile(int capacity);
        Pile(int* tab, int size, int capacity);
        Pile(const Pile&);
        ~Pile();

        int capacity() const;
        int size() const;
        bool empty() const;
        void push(int v);
        void pop();
        int top();

        int& operator[](int index) const;
};

std::ostream& operator<< (std::ostream& out, const Pile& pile);

#endif