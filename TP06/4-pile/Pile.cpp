#include "Pile.hpp"

Pile::Pile(int* tab, int size, int capacity): _tab(tab), _size(size), _capacity(capacity)
{
    if (_capacity <= 0)
    {
        throw std::invalid_argument("size");
    }
}

Pile::Pile(int capacity): Pile(capacity <= 0? throw std::invalid_argument("capacity") : new int[capacity], 0, capacity)
{}

Pile::Pile(): Pile(10)
{}

Pile::Pile(const Pile& p): Pile(p._capacity)
{
    _size = p._size;
    memcpy(p._tab, _tab, _size*sizeof(int));
}

Pile::~Pile()
{
    delete [] _tab;
}


int Pile::capacity() const
{
    return _capacity;
}

int Pile::size() const
{
    return _size;
}

bool Pile::empty() const
{
    return _size == 0;
}

void Pile::push(int v)
{
    if (_size == _capacity)
    {
        _capacity *= 2;
        _tab = (int*) realloc(_tab, _capacity);
    }

    _tab[_size++] = v;
}


void Pile::pop()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("size");
    }
    _size--;
}


int Pile::top()
{
    return _tab[_size-1];
}


int& Pile::operator[](int index) const
{
    return _tab[index];
}

std::ostream& operator<< (std::ostream& out, const Pile& pile)
{
    for (int i=0; i< pile.size(); i++)
    {
        out << pile[i];
        out << " ";
    }
    
    return out;
}
