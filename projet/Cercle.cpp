#include "Cercle.hpp"


Cercle::Cercle(Point p, COULEURS couleur, int rayon): Forme(p, couleur, 2*rayon, 2*rayon), _rayon(rayon)
{}

Cercle::Cercle(): Cercle(ORIGINE, COULEURS::BLEU, 0)
{}

int Cercle::getRayon() const
{
    return _rayon;
}

void Cercle::setRayon(int rayon)
{
    _rayon = rayon;
    this->setLargeur(2*_rayon);
    this->setHauteur(2*_rayon);
}

std::ostream& operator<< (std::ostream& out, const Cercle& cercle)
{
    out << "Cercle (" << cercle.getPoint().getX() << " " << cercle.getPoint().getY() << " " << cercle.getRayon() << ")";
    return out;
}

std::string Cercle::toString()
{
    return std::string("Cercle (") + std::to_string(getPoint().getX()) + " " + std::to_string(getPoint().getY()) + " " + std::to_string(getRayon()) + ")";;
}