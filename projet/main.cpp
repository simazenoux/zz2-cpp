#include <iostream>

#include "Cercle.hpp"
#include "Rectangle.hpp"

int main(int, char**)
{
    Rectangle r (Point(),10,20);
    std::cout << r << std::endl;

    Cercle c (Point(15,15), COULEURS::BLEU, 5);
    std::cout << c << std::endl;
    
    return 0;
}