#ifndef CERCLE_HPP
#define CERCLE_HPP

#include "Forme.hpp"

class Cercle: public Forme
{
    private:
        int _rayon;

    public:
        Cercle();
        Cercle(Point p, COULEURS couleur, int rayon);

        int getRayon() const;

        void setRayon(int rayon);

        virtual std::string toString();
};

std::ostream& operator<< (std::ostream& out, const Cercle& cercle);

#endif