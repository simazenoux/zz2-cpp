#include "Rectangle.hpp"


Rectangle::Rectangle(Point point, COULEURS couleur, int width, int height): Forme(point, couleur), _width(width), _height(height)
{}

Rectangle::Rectangle(Point point, int width, int height): Forme(point, COULEURS::NOIR), _width(width), _height(height)
{}

Rectangle::Rectangle(): Forme(ORIGINE, COULEURS::NOIR)
{}

int Rectangle::getWidth() const
{
    return _width;
}

int Rectangle::getHeight() const
{
    return _height;
}

void Rectangle::setWidth(int width)
{
    _width = width;
}

void Rectangle::setHeight(int height)
{
    _height = height;
}

std::string Rectangle::toString()
{
    return std::string("Rectangle (") + std::to_string(getPoint().getX()) + " " + std::to_string(getPoint().getY()) + " " + std::to_string(getHauteur()) + " " + std::to_string(getLargeur()) + ")";;

}

std::ostream& operator<< (std::ostream& out, const Rectangle& rectangle)
{
    out << "Rectangle (" << rectangle.getPoint().getX() << " " << rectangle.getPoint().getY() << " " << rectangle.getWidth() << " " << rectangle.getHeight() << ')';
    return out;
}