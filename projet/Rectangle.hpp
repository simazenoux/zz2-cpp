#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "Forme.hpp"

class Rectangle: public Forme
{
    private:
        int _width;
        int _height;

    public:
        Rectangle();
        Rectangle(Point p, COULEURS couleur, int width, int height);
        Rectangle(Point p, int width, int height);
        
        int getWidth() const;
        int getHeight() const;

        void setWidth(int width);
        void setHeight(int height);

        virtual std::string toString();
};

std::ostream& operator<< (std::ostream& out, const Rectangle& rectangle);

#endif