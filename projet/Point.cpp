#include "Point.hpp"

Point ORIGINE = Point();


Point::Point(int x, int y): _x(x), _y(y)
{}


Point::Point(): Point(0,0)
{}


int Point::getX() const {
    return _x;
}

int Point::getY() const {
    return _y;
}

void Point::setX(int x) {
    _x = x;
}

void Point::setY(int y) {
    _y = y;
}

void Point::deplacerDe(int x, int y) {
    setX(getX()+x);
    setY(getY()+y);
}

void Point::deplacerVers(int x, int y) {
    setX(x);
    setY(y);
}

std::ostream& operator<< (std::ostream& out, const Point& point)
{
    out << "Point(" << point._x << ", " << point._y << ')';
    return out;
}