#include "Forme.hpp"


int Forme::_prochainId = 0;

Forme::Forme(Point p, COULEURS couleur, int hauteur, int largeur): _point(p), _couleur(couleur), _id(_prochainId++), _hauteur(hauteur), _largeur(largeur)
{}


Forme::Forme(Point p, COULEURS couleur): Forme(p,couleur, 0, 0)
{}

Forme::Forme(): Forme(ORIGINE, COULEURS::BLEU)
{}


int Forme::getId() const
{
    return _id;
}

Point& Forme::getPoint()
{
    return _point;
}

Point Forme::getPoint() const
{
    return _point;
}

COULEURS Forme::getCouleur() const
{
    return _couleur;
}

int Forme::getHauteur() const
{
    return _hauteur;
}

int Forme::getLargeur() const
{
    return _largeur;
}


void Forme::setX(int x)
{
    _point.setX(x);
}

void Forme::setY(int y)
{
    _point.setY(y);
}

void Forme::setHauteur(int hauteur)
{
    _hauteur = hauteur;
}

void Forme::setLargeur(int largeur)
{
    _largeur = largeur;
}

void Forme::setPoint(Point point)
{
    _point = point;
}

void Forme::setCouleur(COULEURS couleur)
{
    _couleur = couleur;
}

int Forme::prochainId()
{
    return _prochainId;
}

std::string Forme::toString()
{
    return "Forme (" + std::to_string(getHauteur()) + " " + std::to_string(getLargeur()) + ")";
}