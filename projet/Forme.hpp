#ifndef FORME_HPP
#define FORME_HPP

#include <iostream>
#include <sstream>

#include "Point.hpp"
#include "Couleurs.hpp"

class Forme
{
    private:
        Point _point;
        COULEURS _couleur;
        int _id;
        static int _prochainId;
        int _hauteur;
        int _largeur;

    public:
        Forme();
        Forme(Point p, COULEURS couleur);
        Forme(Point p, COULEURS couleur, int hauteur, int largeur);


        int getId() const;
        Point getPoint() const;
        Point& getPoint();
        COULEURS getCouleur() const;
        int getHauteur() const;
        int getLargeur() const;

        void setX(int x);
        void setY(int y);
        void setPoint(Point p);
        void setCouleur(COULEURS couleur);
        void setHauteur(int hauteur);
        void setLargeur(int largeur);

        virtual std::string toString();

        static int prochainId();
};

#endif